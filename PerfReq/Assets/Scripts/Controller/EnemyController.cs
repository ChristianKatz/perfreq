﻿using UnityEngine;

namespace UEGP3CA.Controller
{
    public class EnemyController : MonoBehaviour
    {
        // change the direction after a fixed time
        private float _changeDirection = 0;
        // signal to change the direction
        private bool _change;
        
        private void Update()
        {
            // change the direction when the value is 0
            if (_changeDirection <= 0)
            {
                _change = true;
            }

            // when the enemy moved for a fixed time in a direction, he will be pushed in the another direction
            if (_change)
            {
                transform.Translate(5 * Time.deltaTime, 0, 0);
                _changeDirection += Time.deltaTime;

                // if the enemy moved for a fixed time in one direction, the direction will be changed
                if (_changeDirection > 2)
                {
                    _change = false;
                }
            }
            
            // when the enemy moved for a fixed time in a direction, he will be pushed in the another direction
            if (!_change)
            {
                transform.Translate(-5 * Time.deltaTime, 0, 0);
                _changeDirection -= Time.deltaTime;
            }
        }
    }
}
